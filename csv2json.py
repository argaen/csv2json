#!/usr/bin/python
# -*- coding: utf-8 -*-

import json
import csv
import ast

def utf_8_encoder(unicode_csv_data):
    for line in unicode_csv_data:
        yield line.encode('utf-8')

if __name__ == "__main__":

    dict_json = {
      "type" : "smartQuiz",
      "displayName": "Parc de Nadal de Reus",
      "id": "1",
      "baseURI": "url",
      "clues": []
    }

    with open('test.csv', 'rb') as f:
        # reader = csv.reader(utf_8_encoder(f))
        reader = csv.reader(f)
        keys = reader.next()

        for l in reader:

            zipped = zip(keys, l)
            json_data = {}
            for e in zipped:
                if len(e[1]) and e[1][0] == '[':
                    json_data[e[0]] = ast.literal_eval(e[1])
                elif len(e[1]) and e[1][0] == '{':
                    json_data[e[0]] =  [ast.literal_eval(e[1])]
                else:
                    json_data[e[0]] = ast.literal_eval('"'+e[1].strip('"')+'"')

            dict_json["clues"].append(json_data)

        print json.dumps(dict_json, ensure_ascii=False, indent=4)
